intro='Hello I am a string Inside Python'
a=intro[0]+intro[-1]
b=intro[4]
print(a)
# print(b)

# for i in range(2,10):
#     print(i)

for i in intro:
    # print(intro)      # returns complete string
    print(i)            # returns characters of string with white spaces
l=len(intro)

for i in range(1,l+1):
    print(intro[-i]) 